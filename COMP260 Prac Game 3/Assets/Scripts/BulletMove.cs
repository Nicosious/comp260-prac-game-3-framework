﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMove : MonoBehaviour
{

    public float speed = 10.0f;
    public Vector3 direction;
    private Rigidbody rigidbody;
    private float timer = 0.0f;
    private float despawnTime = 1.5f;

    // Use this for initialization
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        rigidbody.velocity = speed * direction;
    }

    void OnCollisionEnter(Collision collision)
    {
        //Destroy the bullet
        Destroy(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (timer > despawnTime)
        {
            Destroy(gameObject);
        }
    }
}